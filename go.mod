module gitlab.com/KopfKrieg/yearprogressbot

go 1.12

require (
	gitlab.com/KopfKrieg/gotodon v1.2.0
	gitlab.com/KopfKrieg/gotodonr v1.0.1
)
