# Fediverse Year Progress Bot

This is an implementation of a "year progress bot" for the Fediverse, written in Golang and deployed using Docker. The official bot to follow can be found at [@YearProgress@troet.cafe](https://troet.cafe/@YearProgress).
